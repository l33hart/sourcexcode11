//
//  ViewController.swift
//  FirstProject
//
//  Created by Lee Hart on 4/7/20.
//  Copyright © 2020 Lee Hart. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /**
     Adds two numbers Together and returns the result.
     - parameter num1: The First Number
     - parameter num2: The Second Number
     - returns: the sum of num1 and num2
     -
     */
    func addNumbers(num1: Int, num2: Int) -> Int
    {
        return num1 + num2
        
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

